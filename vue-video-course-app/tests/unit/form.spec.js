import Form from '@/components/contact/Form';
import { mount } from '@vue/test-utils';

function factory(options = {}) {
  return mount(Form, {
    ...options
  });
}

test('for passed in props', () => {
  const { vm } = factory({
    propsData: {
      submitButtonText: 'Test'
    }
  });
  expect(vm.submitButtonText).toBe('Test');
});

test('for name in data', () => {
  const name = 'Lauri';
  const wrapper = factory();
  wrapper.setData({
    name
  });

  expect(wrapper.vm.name).toBe(name);
  const field = wrapper.find('.input#name');
  expect(field.element.value).toBe(name);
});

test('twitter link is right', () => {
  const wrapper = factory();
  wrapper.vm.contact.main.twitter.value = 'foobar';
  expect(wrapper.vm.links).toMatchSnapshot();
});

test('form submits to the outside', () => {
  const wrapper = factory();
  const form = wrapper.find('form');
  form.trigger('submit');
  expect(wrapper.emitted().submit).toBeTruthy();
  expect(wrapper.emitted().submit).toEqual([[wrapper.vm.$data]]);
});
