import Sidebar from '@/components/Sidebar';
import List from '@/components/general/List';
import { mount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import Model from '@codeship/modelist';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VueRouter);

function factory(options = {}) {
  const store = {
    state: {},
    getters: {
      contacts: jest.fn().mockReturnValue(
        new Model({
          data: [
            {
              name: 'Alisa'
            },
            {
              name: 'Karoliina'
            }
          ]
        })
      )
    }
  };
  return mount(Sidebar, {
    store: new Vuex.Store(store),
    router: new VueRouter(),
    localVue,
    ...options
  });
}

test('sidebar', () => {
  const wrapper = factory();
  expect(wrapper.contains(List)).toBe(true);
});

test('snapshot', () => {
  const wrapper = factory();
  expect(wrapper.html()).toMatchSnapshot();
});
