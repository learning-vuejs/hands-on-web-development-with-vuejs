import Vuex from 'vuex';
import { cloneDeep } from 'lodash';
import { structure } from '@/lib/store';
import { collections } from '@/lib/firebase';

jest.mock('@/lib/firebase');
collections.contacts.get.mockResolvedValue([]);
collections.contacts.add.mockImplementation(() => Promise.resolve({ id: 1 }));

function factory() {
  const clone = cloneDeep(structure);
  return new Vuex.Store(clone);
}

describe('mutations', () => {
  test('#addContact', () => {
    const store = factory();
    const expected = { name: 'Edu' };
    store.commit('addContact', expected);
    expect(store.state.contacts.all()).toEqual([expected]);
    expect(store).toMatchSnapshot();
  });
});

describe('state', () => {
  test('contacts is an empty collection', () => {
    const store = factory();
    expect(store.state.contacts.all()).toEqual([]);
  });
});

describe('actions', () => {
  const { actions } = structure;
  test('addContact', async () => {
    const fakeStore = {
      commit: jest.fn()
    };
    const contact = { name: 'Arno' };
    await actions.addContact(fakeStore, contact);
    expect(fakeStore.commit).toHaveBeenCalledWith('addContact', {
      id: 1,
      ...contact
    });
  });
});
